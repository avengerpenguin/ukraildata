#!/bin/sh

cat railways | while read name ; do

    url="http://en.wikipedia.org/wiki/Special:Export/$name"
    type=$(wget -O - "$url" 2>/dev/null | grep '.*type.*=.*' | sed 's/.*= *//g')

    echo $name '\t' $type

done
